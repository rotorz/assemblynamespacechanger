﻿// Copyright (c) Rotorz Limited. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root.

using Mono.Options;
using System;
using System.Collections.Generic;

namespace AssemblyNamespaceChanger {

	class Program {

		static void Main(string[] args) {
			ChangeNamespaceCommand.Parameters parameters = new ChangeNamespaceCommand.Parameters();
			bool showHelp = false;

			var options = new OptionSet() {
				{ "i|input=", "Path to the input assembly.", input => parameters.InputAssemblyPath = input },
				{ "o|output=", "Path to the output assembly.", output => parameters.OutputAssemblyPath = output },
				{ "s|source-namespace=", "The old namespace that is to be renamed.", sourceNamespace => parameters.SourceNamespace = sourceNamespace },
				{ "t|target-namespace=", "The new namespace.", targetNamespace => parameters.TargetNamespace = targetNamespace },
				{ "n|output-assembly-name=", "Overrides name of output assembly.", name => parameters.OutputAssemblyName = name },
				{ "h|help", "Show this message and then exit.", value => showHelp = value != null },
			};

			try {
				List<string> extraArgs = options.Parse(args);
				
                if (showHelp) {
					options.WriteOptionDescriptions(Console.Out);
					return;
				}

				var command = new ChangeNamespaceCommand(parameters);
				command.Execute();
			}
			catch (OptionException ex) {
				OutputError(ex);
			}
			catch (ProgramErrorException ex) {
				OutputError(ex);
			}
        }

		private static void OutputError(Exception error) {
			Console.Error.WriteLine(error.Message);
		}

	}

}
