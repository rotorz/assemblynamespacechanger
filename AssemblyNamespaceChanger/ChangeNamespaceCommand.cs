﻿// Copyright (c) Rotorz Limited. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root.

using Mono.Cecil;
using System.IO;
using System.Text.RegularExpressions;

namespace AssemblyNamespaceChanger {

	public sealed class ChangeNamespaceCommand {

		public struct Parameters {

			public string InputAssemblyPath { get; set; }
			public string OutputAssemblyName { get; set; }
			public string OutputAssemblyPath { get; set; }
			public string SourceNamespace { get; set; }
			public string TargetNamespace { get; set; }

		}

		private readonly Parameters _parameters;

		public ChangeNamespaceCommand(Parameters parameters) {
			_parameters = parameters;
		}

		public void Execute() {
			if (!ValidateParameters())
				return;

			// Avoid redundant conversion!
			if (_parameters.SourceNamespace == _parameters.TargetNamespace)
				return;

			// Resolve relative file paths.
			string resolvedInputAssemblyPath = ResolveRelativePath(Directory.GetCurrentDirectory(), _parameters.InputAssemblyPath);
			var assembly = AssemblyDefinition.ReadAssembly(resolvedInputAssemblyPath);

			ChangeNamespace(assembly, _parameters.SourceNamespace, _parameters.TargetNamespace);

			WriteOutputAssembly(assembly);
		}

		private const string ValidNamespacePattern = @"^([A-Za-z@_][A-Za-z@_0-9]*(\.[A-Za-z@_][A-Za-z@_0-9]*)+)?$";

		private bool ValidateParameters() {
			if (string.IsNullOrEmpty(_parameters.InputAssemblyPath))
				throw new ProgramErrorException("Invalid input assembly path.");
			if (string.IsNullOrEmpty(_parameters.OutputAssemblyPath))
				throw new ProgramErrorException("Invalid output assembly path.");
			if (_parameters.SourceNamespace == null || !Regex.IsMatch(_parameters.SourceNamespace, ValidNamespacePattern, RegexOptions.Compiled))
				throw new ProgramErrorException("Invalid source namespace.");
			if (_parameters.TargetNamespace == null || !Regex.IsMatch(_parameters.TargetNamespace, ValidNamespacePattern, RegexOptions.Compiled))
				throw new ProgramErrorException("Invalid output namespace.");

			if (_parameters.OutputAssemblyPath == _parameters.InputAssemblyPath)
				throw new ProgramErrorException("Cannot overwrite the original assembly!");

			return true;
		}
		
		public static string ResolveRelativePath(string referencePath, string relativePath) {
			return Path.GetFullPath(Path.Combine(referencePath, relativePath));
		}

		private void ChangeAssemblyName(AssemblyDefinition assembly, string newAssemblyName) {
			if (assembly.Name.Name == newAssemblyName)
				return;

            assembly.Name.Name = newAssemblyName;
			assembly.MainModule.Name = newAssemblyName;
		}

		private void ChangeNamespace(AssemblyDefinition assembly, string oldNamespace, string newNamespace) {
			if (oldNamespace == newNamespace)
				return;

			if (oldNamespace != "" && oldNamespace.EndsWith("."))
				oldNamespace += ".";
			if (newNamespace != "" && newNamespace.EndsWith("."))
				newNamespace += ".";

			foreach (var type in assembly.MainModule.Types) {
				if (!type.Namespace.StartsWith(oldNamespace) || type.Name.StartsWith(newNamespace))
					continue;

				type.Namespace = type.Namespace.Replace(oldNamespace, newNamespace);
			}
		}

		private void WriteOutputAssembly(AssemblyDefinition assembly) {
			// Update the name of the output assembly.
			string outputAssemblyName = _parameters.OutputAssemblyName;
			if (string.IsNullOrEmpty(outputAssemblyName)) {
				// Assume filename of output DLL for assembly name if the assembly name
				// of the input DLL was the same as the assembly name of the input DLL.
				if (assembly.Name.Name == Path.GetFileNameWithoutExtension(_parameters.InputAssemblyPath)) {
					outputAssemblyName = Path.GetFileNameWithoutExtension(_parameters.OutputAssemblyPath);
				}
			}
			if (!string.IsNullOrEmpty(outputAssemblyName)) {
				ChangeAssemblyName(assembly, outputAssemblyName);
			}

			string resolvedOutputAssemblyPath = ResolveRelativePath(Directory.GetCurrentDirectory(), _parameters.OutputAssemblyPath);
            assembly.Write(resolvedOutputAssemblyPath);
		}

	}

}
