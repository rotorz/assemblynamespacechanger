﻿// Copyright (c) Rotorz Limited. All rights reserved.
// Licensed under the MIT license. See LICENSE file in the project root.

using System;

namespace AssemblyNamespaceChanger {

	public class ProgramErrorException : Exception {

		public ProgramErrorException(string message)
			: base(message)
		{
		}

	}

}
