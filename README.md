# README

This repository contains the source code for **AssemblyNamespaceChanger**; a command line
tool for changing a namespace in an assembly.

Licensed under the MIT license. See LICENSE file in the project root for full license
information. DO NOT contribute to this project unless you accept the terms of the
contribution agreement.


## Warning

Make sure that you backup your files before using this tool.


## Command Line Options

- **-i** or **--input={PATH}** - Path to the input assembly.

- **-o** or **--output={PATH}** - Path to the output assembly.

- **-s** or **--source-namespace={NAMESPACE}** - The old namespace that is to be renamed.

- **-t** or **--target-namespace={NAMESPACE}** - The new namespace.

- **-h** or **--help** - Shows help information and then exit.


## Usage Example

The following command is broken onto multiple lines so that it is easier to read:

```
AssemblyNamespaceChanger.exe
   -i="c:\example\SomeAssembly.dll"
   -t="c:\example\SomeAssemblyNew.dll"
   -s=SomeAssembly.OldNamespace
   -t=SomeAssemblyNew.With.NewNamespace
```

Useful links
------------

- [Rotorz Website](<http://rotorz.com>)

Contribution Agreement
----------------------

This project is licensed under the MIT license (see LICENSE). To be in the best
position to enforce this license the copyright status of this project needs to
be as simple as possible. To achieve this the following terms and conditions
must be met:

- All contributed content (including but not limited to source code, text,
  image, videos, bug reports, suggestions, ideas, etc.) must be the
  contributors own work.

- The contributor disclaims all copyright and accepts that their contributed
  content will be released to the public domain.

- The act of submitting a contribution indicates that the contributor agrees
  with this agreement. This includes (but is not limited to) pull requests, issues,
  tickets, e-mails, newsgroups, blogs, forums, etc.